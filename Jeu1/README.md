# Adenvturen't capitalist

Basé sur [cette vidéo](https://www.youtube.com/watch?v=qCA7FBwKOgI)

Modifié pour utiliser des listes

## Installation

- Installer [Python](https://python.org)
- Cloner le repo

``` sh
git clone https://framagit.com/loulou310/jeu-pygame
```

- Installer les dépendances

``` sh
pip install -r requirements.txt
```

Il n'est pas nécessaire d'installer la police.

Vous pouvez mainteant lancer le jeu avec la commande `python3 Jeu.py`